<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['web', 'auth'])->group(function () {

	Route::get('profile', 'ProfileController@show')->name('profile');
	Route::put('profile', 'ProfileController@update');
	Route::get('profile/edit', 'ProfileController@edit');
	Route::delete('profile/image', 'ProfileController@remove');

	Route::get('account', 'AccountController@show')->name('account');
	Route::put('account', 'AccountController@update');
	Route::get('account/delete', 'AccountController@delete');
	Route::delete('account/delete', 'AccountController@destroy');
});

//Artikel
Route::resource('posts', 'PostController');
Route::get('posts/hapus/{id}', 'PostController@destroy')->name('posts.hapus');

//Kategori
Route::resource('category', 'CategoryController');
Route::get('/category/hapus/{id}', 'CategoryController@destroy')->name('category.hapus');

//Tags
Route::resource('tags', 'TagController');
Route::get('/tags/hapus/{id}', 'TagController@destroy')->name('tags.hapus');
