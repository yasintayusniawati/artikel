@extends('layouts.app')

{{-- @section('title', 'Buat Kategori Artikel') --}}
@section('content')
    <div class="container">
        <div class="card mt-4">
            <div class="card-header">
                <div class="text-center">Edit Tag Artikel</div>    
            </div>
            <div class="card-body">
                <form action="{{route('tags.update', $tag->id)}}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="title">Nama Tag</label>
                        <input type="text" name="name" class="form-control" placeholder="Nama Tag Artikel..." value="{{$tag->name}}">
                        {{-- validator --}}
                        @if($errors->has('name'))
                            <div class="text-danger">
                                {{$errors->first('name')}}
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-success" value="Simpan">
                    </div>
                </form>
            </div>    
        </div>    
    </div>
@endsection