@extends('layouts.app')

{{-- @section('title', 'Edit Artikel') --}}
@section('content')
    <div class="container">
        <div class="card mt-4">
            <div class="card-header">
                <div class="text-center">Edit Artikel Baru</div>    
            </div>
            <div class="card-body">
                <form action="{{route('posts.update', $posts->id)}}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="title">Judul Artikel</label>
                        <input type="text" name="title" class="form-control" value="{{$posts->title}}" placeholder="Judul Artikel...">
                        {{-- validator --}}
                        @if($errors->has('title'))
                            <div class="text-danger">
                                {{$errors->first('title')}}
                            </div>
                        @endif
                        <select name="category_id" id="" class="form-control">
                            @foreach ($category as $c)
                                <option value="{{$c->id}}" {{ $c->id == $posts->category_id ? 'selected' : '' }}>{{$c->name}}</option>
                            @endforeach
                        </select>
                        <label for="content">Konten</label>
                        <textarea name="content" class="form-control" placeholder="Konten Artikel...">{{$posts->content}}</textarea>
                        {{-- validator --}}
                        @if($errors->has('title'))
                            <div class="text-danger">
                                {{$errors->first('title')}}
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-success" value="Simpan">
                    </div>
                </form>
            </div>    
        </div>    
    </div>    
@endsection