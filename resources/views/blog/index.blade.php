@extends('layouts.app')

@section('title', 'Buat Artikel')
@section('content')
    <div class="container">
        <div class="card mt-4">
            <div class="card-header">
                <div class="text-center">Artikel</div>    
            </div>
            <div class="card-body">
                <a href="{{route('posts.create')}}" class="btn btn-primary">Buat Artikel</a>
                <div class="text-center"><h4>List Artikel</h4></div>
                <table class="table table-bordered table-hover table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Judul Artikel</th>
                            <th>Konten Artikel</th>
                            <th>Kategori</th>
                            <th>Opsi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $i=1
                        @endphp
                        @foreach($posts as $p)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$p->title}}</td>
                                <td>{{$p->content}}</td>
                                <td>{{$p->categories['name']}}</td>
                                <td>
                                    <a href="{{route('posts.edit', $p->id)}}" class="btn btn-primary"><i class="fa fa-edit"></i> Edit</a>
                                    <a href="{{route('posts.hapus', $p->id)}}" class="btn btn-danger"><i class="fa fa-trash"></i> Hapus</a>
                                </td>
                            </tr>
                        @endforeach   
                    </tbody>
                </table>    
            </div>    
        </div>    
    </div>    
@endsection