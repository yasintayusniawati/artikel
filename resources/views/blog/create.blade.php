@extends('layouts.app')

@section('title', 'Buat Artikel')
@section('content')
    <div class="container">
        <div class="card mt-4">
            <div class="card-header">
                <div class="text-center">Buat Artikel Baru</div>    
            </div>
            <div class="card-body">
                <form action="{{route('posts.store')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="title">Judul Artikel</label>
                        <input type="text" name="title" class="form-control" placeholder="Judul Artikel...">
                        {{-- validator --}}
                        @if($errors->has('title'))
                            <div class="text-danger">
                                {{$errors->first('title')}}
                            </div>
                        @endif
                        <label for="category">Kategori</label>
                        <select name="category_id" id="" class="form-control">
                            <option value="" class="disable selected">Pilih Kategori</option>
                            @foreach ($category as $c)
                                <option value="{{$c->id}}">{{$c->name}}</option>
                            @endforeach
                        </select>
                        {{-- validator --}}
                        @if($errors->has('category_id'))
                            <div class="text-danger">
                                {{$errors->first('category_id')}}
                            </div>
                        @endif
                        {{-- Masih belum bisa muncul wkwkwk --}}
                        {{-- <label for="tags">Tag</label>
                        <select name="tags[]" id=""  class="form-control select2" multiple="">
                            @foreach ($tags as $t)
                                <option value="{{$t->id}}">{{$t->name}}</option>
                            @endforeach
                        </select> --}}
                        <label for="image">Gambar</label>
                        <input type="file" name="image" class="form-control">
                        <label for="content">Konten</label>
                        <textarea name="content" class="form-control" placeholder="Konten Artikel..."></textarea>
                        {{-- validator --}}
                        @if($errors->has('content'))
                            <div class="text-danger">
                                {{$errors->first('content')}}
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-success" value="Simpan">
                    </div>
                </form>
            </div>    
        </div>    
    </div>   
@endsection

{{-- Kok belum bisa muncul wkwk --}}
{{-- @section('js')
    <script>
        CKEDITOR.replace( 'content' );
    </script>
@endsection --}}