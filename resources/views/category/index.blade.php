@extends('layouts.app')

{{-- @section('title', 'Buat Kategori Artikel') --}}
@section('content')
    <div class="container">
        <div class="card mt-4">
            <div class="card-header">
                <div class="text-center">Kategori Artikel</div>    
            </div>
            <div class="card-body">
                <a href="{{route('category.create')}}" class="btn btn-primary">Buat Kategori</a>
                <div class="text-center"><h4>List Kategori</h4></div>
                <table class="table table-bordered table-hover table-striped" id="data-table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Kategori</th>
                            <th>Tanggal Edit</th>
                            <th>Opsi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $i=1
                        @endphp
                        @foreach($category as $c)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$c->name}}</td>
                                <td>{{date('j F Y', strtotime($c->updated_at))}}</td>
                                <td>
                                    <a href="{{route('category.edit', $c->id)}}" class="btn btn-primary"><i class="fa fa-edit"></i> Edit</a>
                                    <a href="{{route('category.hapus', $c->id)}}" class="btn btn-danger"><i class="fa fa-edit"></i> Hapus</a>
                                    {{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editModal">
                                        <i class="fa fa-edit"></i> Edit
                                    </button>
                                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#hapusModal">
                                        <i class="fa fa-trash"></i> Hapus
                                    </button> --}}
                                </td>
                            </tr>
                        @endforeach   
                    </tbody>
                </table>
            </div>    
        </div>    
    </div>
@endsection