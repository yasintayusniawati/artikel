@extends('layouts.app')

{{-- @section('title', 'Buat Kategori Artikel') --}}
@section('content')
    <div class="container">
        <div class="card mt-4">
            <div class="card-header">
                <div class="text-center">Buat Kategori Artikel</div>    
            </div>
            <div class="card-body">
                <form action="{{route('category.store')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="title">Nama Kategori</label>
                        <input type="text" name="name" class="form-control" placeholder="Nama Kategori Artikel...">
                        {{-- validator --}}
                        @if($errors->has('name'))
                            <div class="text-danger">
                                {{$errors->first('name')}}
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-success" value="Simpan">
                    </div>
                </form>    
        </div>    
    </div>
@endsection