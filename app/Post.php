<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'posts';
    protected $fillable = [
        'title', 'slug', 'content', 'image', 'category_id'
    ];
    public function categories(){
        return $this->belongsTo('App\Category', 'category_id');
    }

    public function tags(){
        return $this->belongsToMany('App\Tag');
    }
}
